﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogiqueMetierHotel
{
    public class Reservation
    {
        public static int idReservation = 0;
        public String nomClient;
        public String prenomClient;
        public Client client;
        public DateTime dateArrivee;
        public DateTime dateDepart;
        public int numCarteBancaire;
        public int nbPersonne;
        public int dureeSejour;
        public double prixTotal;

        public Reservation(String newNomClient, String newPrenomClient, int newNumCarteBancaire, DateTime newDateArrivee, DateTime newDateDepart, int newNbPersonne, double PrixTotal)
        {
            idReservation += 1;
            this.nomClient = newNomClient;
            this.prenomClient = newPrenomClient;
            this.dateArrivee = newDateArrivee;
            this.dateDepart = newDateDepart;
            this.numCarteBancaire = newNumCarteBancaire;
            this.nbPersonne = newNbPersonne;
            this.dureeSejour = (dateDepart - dateArrivee).Days;
            this.prixTotal = PrixTotal;
            this.client = new Client(newNomClient, newPrenomClient, newNumCarteBancaire);
        }

        public Reservation()
        {

        }

        public override string ToString()
        {
            return  this.nomClient + "\n - " + this.prenomClient + "\n - " + this.dateArrivee + "\n - " + this.dateDepart + "\n - " + "Prix total de votre réservation : " + this.prixTotal;
        }

        public void reservationHotel(List<Hotel> resList, List<Hotel> baseList)
        {
            String nom;
            String nomPersonne;
            String prenom;
            int numeroCarte;
            int nbPersonne;
            DateTime dateArrivee;
            DateTime dateDepart;
            Console.WriteLine("============= INDIQUER L'HOTEL QUI VOUS CORRESPOND =================");
            nom = Console.ReadLine();

            Console.WriteLine("=======  MERCI DE BIEN SAISIR DANS L'ORDRE LES INFORMATIONS NECESSAIRES A UNE RESERVATION  ====== "+"\n" + " NOM :" +"\n" + " PRENOM :" +"\n" + " NUMERO CB :" +"\n" +" NB PERSONNES :" +"\n" + " DATE DEBUT :" +"\n" +" DATE FIN :");
            nomPersonne = Console.ReadLine();
            prenom = Console.ReadLine();
            numeroCarte = Convert.ToInt32(Console.ReadLine());
            nbPersonne = Convert.ToInt32(Console.ReadLine());
            dateArrivee = Convert.ToDateTime(Console.ReadLine());
            dateDepart = Convert.ToDateTime(Console.ReadLine());

            Chambre z = null;

            foreach (Hotel x in baseList)
            {
                if (x.nomHotel.Equals(nom))
                {
                    TimeSpan total = (dateDepart - dateArrivee);
                    double temp = total.TotalDays;
                    Console.WriteLine(temp);
                    double prixTotal = (int)(x.prixNuit * nbPersonne) * temp;
                    Reservation res = new Reservation(nomPersonne, prenom, numeroCarte, dateArrivee, dateDepart, nbPersonne, prixTotal);
                    
                    Chambre chambre = x.Reserver(res);

                    if (chambre.Equals(z)==false)
                    {
                       
                        Console.WriteLine(x.ToString());
                        //info chambre
                        Console.WriteLine(chambre.ToString());
                        //info reservation
                        chambre.ToStringListReservation();
                        Console.WriteLine("RESERVATION EFFECTUE");
                    }
                    else
                    {
                        Console.WriteLine("PAS DE CHAMBRE DISPONIBLE DESOLER");
                    }


                }
            }
        }

    }
}
