﻿using System;
using System.Collections.Generic;
using System.Text;


/*

 Implémentation de la classe client qui illustre les informations d'un client comme l'identifiant 
 le nom du client et le prenom de la personne à hébéerger 
 et aussi le numero de la carte de paiement



*/

namespace LogiqueMetierHotel
{
    public class Client
    {
        // identifiant client qui sére à gerer et differencier un client d'un autre
        public static int idClient = 0;
        
        // Nom du client 
        public String nomClient;
        
        // prénom du client 
        public String prenomClient;
        
        // le numero de la carte bancaire 
        public int numeroCarteBancaire;


        // constructeur client 
        public Client(String newNomClient, String newPrenomClient, int newNumeroCarteBancaire)
        {
            idClient += 1;
            this.nomClient = newNomClient;
            this.prenomClient = newPrenomClient;
            this.numeroCarteBancaire = newNumeroCarteBancaire;
        }

        public Client()
        {

        }



    }
}
