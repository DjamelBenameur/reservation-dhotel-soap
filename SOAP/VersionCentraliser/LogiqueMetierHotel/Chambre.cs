﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 La classe qui implémente une chambre d'hotel avec comme paramettre
 le numero de la chambre et le nombre de lits 

*/

namespace LogiqueMetierHotel
{
    public class Chambre
    {
        public int numChambre;
        public int nbLits;
        


        // liste des réservations
        public List<Reservation> ListReservations = new List<Reservation>();

        
        // constructeur avec et sans parametres 
        public Chambre() { }
        public Chambre(int num, int nbLits)
        {
            this.numChambre = num;
            this.nbLits = nbLits;        
        }

        

        // fonction ajout réservation qui ajoute une reservation à une liste de réservations

        public void ajoutReservation(Reservation r)
        {
            this.ListReservations.Add(r);
        }

        




        // Fonction qui enleve une reservation

        public void retraitReservation(Reservation r)
        {
            if (DateTime.Now > r.dateDepart)
            {
                this.ListReservations.Remove(r);
            }
        }

        




        // fonction qui affiche une liste de réservation
        public void ToStringListReservation()
        {
            foreach (Reservation z in this.ListReservations)
            {
                Console.WriteLine(z.ToString());
            }
        }

        


        


        // Fonction est disponible qui test la coéherance des données comme le fait que le dates ne se chauvauchent pas 
        // et le nombre de lits est disponible 
        
        public bool estDisponible(DateTime debut, DateTime fin, int nbLits)
        {
            
            if (this.nbLits < nbLits)
            {
                return false;
            }

            
            if (ListReservations.Count == 0)
            {
                return true;
            }

            else
            {
                foreach (Reservation r in this.ListReservations)
                {
                    DateTime dateDebut = r.dateArrivee;
                    DateTime dateFin = r.dateDepart;

                     bool occupee =((debut.CompareTo(dateDebut) >= 0 & fin.CompareTo(dateFin) <= 0)
                                   | (debut.CompareTo(dateDebut) < 0 & fin.CompareTo(dateFin) > 0)
                                   | (debut.CompareTo(dateDebut) < 0 & fin.CompareTo(dateFin) <= 0)
                                   | (debut.CompareTo(dateDebut) >= 0 & fin.CompareTo(dateFin) < 0)
                                   );
                    
                    if (occupee)
                    {
                        return false;
                    }
                }
            }

           
            return true;
        }




        public override string ToString()
        {
            return "Vous pouvez choisir la chambre numéro " + this.numChambre + " et " + "qui possede : " + this.nbLits + " lits";
        }
    }
}
