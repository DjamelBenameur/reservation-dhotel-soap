﻿using System;
using System.Collections.Generic;

namespace LogiqueMetierHotel
{
    class Program
    {

        static void Main(string[] args)
        {
            Hotel SOFITEL = new Hotel("HOTEL SOFITEL ALGER", "12 RUE HASSIBA BEN BOUALI", "BELOUIZDAD", "ALGERIE", 5, 100);
            List<Hotel> baseList = new List<Hotel>();
            baseList.Add(SOFITEL);
            List<Hotel> research = baseList;





            Console.WriteLine("===================== BIENVENUE DANS NOTRE HOTEL ===============================\n");
            Console.WriteLine(SOFITEL);
            SOFITEL.InitChambre();

            Console.WriteLine("===================== LES CHAMBRES ACTUELLEMENT DISPONIBLES :==================\n");
            SOFITEL.afficherChambre();
            Console.WriteLine("\n");

            String cmd = "10";
            while (cmd != "0")
            {
                Console.WriteLine("VOTRE CHOIX SVP :" + "\n" +  "0 : QUITTER " + "\n" + "1 : RECHERCHER  " + "\n" + "2 : RESERVER");
                cmd = Console.ReadLine();
                switch (cmd)
                {
                    case "0":
                        Console.WriteLine("Merci !");
                        break;
                    case "1":
                        Console.WriteLine("\n");
                        Console.WriteLine("==========  TOUS LES HOTELS ACTUELLEMENT DISPONIBLES =========== ");
                        Console.WriteLine("\n");
                        Recherche x = new Recherche();
                        x.ToStringList(research);
                        x.rechercheHotel(research);
                        break;
                    case "2":
                        Reservation y = new Reservation();
                        y.reservationHotel(research,baseList);
                        research = baseList;
                        break;
                }
            }
        }
    }
}
